//
//  OgreHelper.h
//  OgreTest
//
//  Created by Dev on 2/22/14.
//
//

#ifndef OgreTest_OgreHelper_h
#define OgreTest_OgreHelper_h

#include <OgreVector3.h>
#include <OgreVector2.h>
#include <OgreCompositorManager.h>

class OgreHelper
{
public:
    
    enum EEasingMode
    {
        EM_Quadratic,
        // TODO: EM_Bezier, EM_Hermite, EM_Circular, EM_FullCosine, EM_HalfCosine
    };
    
    // linear interpolation between 2 3D position
    static Ogre::Vector3 lerp (const Ogre::Vector3 &srcPos, const Ogre::Vector3 &destPos, const double deltaTime)
    {
        return srcPos + (destPos - srcPos) * deltaTime;
    }
    
    // interpolate with different easing modes
    static Ogre::Vector3 interpolate (const Ogre::Vector3 &srcPos, const Ogre::Vector3 &destPos, const double t, const EEasingMode easing )
    {
        double time = 0.0f;
        
        switch (easing)
        {
            case EM_Quadratic:
                time = t * t;
                break;
            default:
                time = t;
        }
        
        return srcPos + (destPos - srcPos) * time;
    }
    
    
    // Built in angleTo does not use negative angles, we need it for correct rotation direction
    // Returns the angle between 2 vectors. The angle is comprised between -PI and PI.
    static Ogre::Radian angleTo2 (const Ogre::Vector2 &v1, const Ogre::Vector2 &v2)
    {
        Ogre::Radian angle = v1.angleBetween(v2);
		
        if (v1.crossProduct(v2)<0)
            angle =  -angle;
        
        return angle;

    }
    
    // Compiles the given material's script. Needed bacuse ogre does not load automatically the
    // materials from the file, even it recognizes as a resource
    static void parseMaterialScript (const std::string fileName)
    {
        Ogre::DataStreamPtr tmp = Ogre::ResourceGroupManager::getSingleton().openResource(fileName+".material");
        Ogre::MaterialManager::getSingleton().parseScript(tmp, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    }

    // Compiles the given compositor"s script. Needed bacuse ogre does not load automatically the
    // compositors from the file, even it recognizes as a resource
    static void parseCompositorScript (const std::string fileName)
    {
        Ogre::DataStreamPtr tmp = Ogre::ResourceGroupManager::getSingleton().openResource(fileName+".compositor");
        Ogre::CompositorManager::getSingleton().parseScript(tmp, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    }
    
    
    // Nicer initialization for std::maps
    template <typename T, typename U>
    class create_map
    {
    private:
        std::map<T, U> m_map;
    public:
        create_map(const T& key, const U& val)
        {
            m_map[key] = val;
        }
        
        create_map<T, U>& operator()(const T& key, const U& val)
        {
            m_map[key] = val;
            return *this;
        }
        
        operator std::map<T, U>()
        {
            return m_map;
        }
    };
    
};

#endif
