//
//  Imports .obj models.
//
//
#include <vector>
#include <OgreRoot.h>
#include <OgreString.h>
#include <OgreVector3.h>

#ifndef ios_ogre_ModelImporter_h
#define ios_ogre_ModelImporter_h

struct Vertex
{
    Vertex(const Ogre::Vector3 pos, const Ogre::Vector3 norm)
    :Position(pos), Normal(norm)
    {}
    
    Ogre::Vector3 Position;
    Ogre::Vector3 Normal;
};

class ModelImporter : public Ogre::Singleton<ModelImporter>
{
public:
    
    // Loads the .obj model from media/models folder identified by its name
    bool                        loadObj(const std::string objName);
    
    
    // Creates a mesh from the vertices and faces vectors with the given parameters
    // default material is flat grey
    // Use before CreateEntityAndNode
    void                        createMesh(const std::string meshName, const std::string materialName="");

    // Creates temporary entity and a SceneNode, defined by the parameters, returns the node
    // Use after CreateMesh
    Ogre::SceneNode*            createEntityAndNode(Ogre::SceneManager* sMgr, Ogre::SceneNode* parentNode, std::string entityName,
                                                    const std::string meshName);
    
    
    // Creates a mesh from the vertices and faces vectors with the given parameters, returns a new SceneNode
    // default material is flat grey
    // Use for single object creation
    Ogre::SceneNode*            createMeshAndNode(Ogre::SceneManager* sMgr, Ogre::SceneNode* parentNode, const Ogre::Vector3 pos,
                                                  const std::string materialName = "");

    
private:
    
    Ogre::String                meshName;
    std::vector<Vertex>         vertices;
    std::vector<unsigned short> indices;
    
    // for bounding box
    Ogre::Vector3               bbMinVertices;
    Ogre::Vector3               bbMaxVertices;
};

#endif
