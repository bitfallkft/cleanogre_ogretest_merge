//
//  EffectManager.h
//  OgreTest
//
//  Created by Dev on 2/28/14.
//
//

#ifndef OgreTest_EffectManager_h
#define OgreTest_EffectManager_h

#include <OgreRoot.h>
#include <OgreGpuProgramManager.h>
#include <OgreGpuProgramParams.h>
#include <OgreMaterial.h>
#include <OgreRectangle2D.h>

enum EPostProcessEffect
{
    PPE_GrayScale,
    PPE_Glass,
    PPE_Gamma
};


class EffectManager : public Ogre::Singleton<EffectManager>
{
public:
    
    EffectManager(Ogre::RenderWindow* rw, Ogre::SceneManager* sceneManager, Ogre::Camera* camera);
    ~EffectManager();
    
    void addPostProcessEffect(EPostProcessEffect effect);

private:
    
    void                        _init();
    
    
    Ogre::RenderWindow*         mRenderWindow;
    Ogre::SceneManager*         mSceneManager;
    Ogre::Camera*               mCamera;
    Ogre::TexturePtr            mRenderTexturePtr;
    Ogre::Rectangle2D*          mFullScreenQuad;
    Ogre::Viewport*             mRTTViewPort;
    Ogre::RenderTexture*        mTextureRenderTarget;
    std::string                 mEffectArray[10];
};

#endif
