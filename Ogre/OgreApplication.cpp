//
//         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                     Version 2, December 2004
//
//  Copyright (C) 2013 Clodéric Mars <cloderic.mars@gmail.com>
//
//  Everyone is permitted to copy and distribute verbatim or modified
//  copies of this license document, and changing it is allowed as long
//  as the name is changed.
//
//             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//   0. You just DO WHAT THE FUCK YOU WANT TO.

#include "OgreApplication.h"
#include "ModelImporter.h"
#include <macUtils.h>

OgreApplication::OgreApplication()
: mCameraController(0)
, mRoot(0)
, mSceneManager(0)
, mRenderWindow(0)
, mViewport(0)
, mSelectedEntity(0)
, mLastSelectedEntity(0)
, m_bPostProcess(false)
, mEffectManager(0)
{
    mResourcesRoot = Ogre::macBundlePath() + "/";
    m_FrameEvent = Ogre::FrameEvent();
}

////////////////////////////////////////////////

OgreApplication::~OgreApplication()
{
    stop();
}

////////////////////////////////////////////////

bool OgreApplication::initializeRTShaderSystem()
{
    if (Ogre::RTShader::ShaderGenerator::initialize())
    {
        mShaderGenerator = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
        
        mShaderGenerator->addSceneManager(mSceneManager);
        
        // Setup core libraries and shader cache path.
        Ogre::StringVector groupVector = Ogre::ResourceGroupManager::getSingleton().getResourceGroups();
        Ogre::StringVector::iterator itGroup = groupVector.begin();
        Ogre::StringVector::iterator itGroupEnd = groupVector.end();
        Ogre::String shaderCoreLibsPath;
        Ogre::String shaderCachePath;
        
        for (; itGroup != itGroupEnd; ++itGroup)
        {
            Ogre::ResourceGroupManager::LocationList resLocationsList = Ogre::ResourceGroupManager::getSingleton().getResourceLocationList(*itGroup);
            Ogre::ResourceGroupManager::LocationList::iterator it = resLocationsList.begin();
            Ogre::ResourceGroupManager::LocationList::iterator itEnd = resLocationsList.end();
            bool coreLibsFound = false;
            
            // Try to find the location of the core shader lib functions and use it
            // as shader cache path as well - this will reduce the number of generated files
            // when running from different directories.
            for (; it != itEnd; ++it)
            {
                if ((*it)->archive->getName().find("RTShaderLib") != Ogre::String::npos)
                {
                    shaderCoreLibsPath = (*it)->archive->getName() + "/";
                    shaderCachePath = shaderCoreLibsPath;
                    coreLibsFound = true;
                    break;
                }
            }
            // Core libs path found in the current group.
            if (coreLibsFound)
                break;
        }
        
        // Core shader libs not found -> shader generating will fail.
        if (shaderCoreLibsPath.empty())
            return false;
        
        // Create and register the material manager listener.
        mMaterialMgrListener = new ShaderGeneratorTechniqueResolverListener(mShaderGenerator);
        Ogre::MaterialManager::getSingleton().addListener(mMaterialMgrListener);
        
        Ogre::MaterialPtr baseWhite = Ogre::MaterialManager::getSingleton().getByName("BaseWhite", Ogre::ResourceGroupManager::INTERNAL_RESOURCE_GROUP_NAME);
        baseWhite->setLightingEnabled(false);
        mShaderGenerator->createShaderBasedTechnique(
                                                     "BaseWhite",
                                                     Ogre::MaterialManager::DEFAULT_SCHEME_NAME,
                                                     Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
        mShaderGenerator->validateMaterial(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME,
                                           "BaseWhite");
        baseWhite->getTechnique(0)->getPass(0)->setVertexProgram(
                                                                 baseWhite->getTechnique(1)->getPass(0)->getVertexProgram()->getName());
        baseWhite->getTechnique(0)->getPass(0)->setFragmentProgram(
                                                                   baseWhite->getTechnique(1)->getPass(0)->getFragmentProgram()->getName());
        
        // creates shaders for base material BaseWhiteNoLighting using the RTSS
        mShaderGenerator->createShaderBasedTechnique(
                                                     "BaseWhiteNoLighting",
                                                     Ogre::MaterialManager::DEFAULT_SCHEME_NAME,
                                                     Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
        mShaderGenerator->validateMaterial(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME,
                                           "BaseWhiteNoLighting");
        Ogre::MaterialPtr baseWhiteNoLighting = Ogre::MaterialManager::getSingleton().getByName("BaseWhiteNoLighting", Ogre::ResourceGroupManager::INTERNAL_RESOURCE_GROUP_NAME);
        baseWhiteNoLighting->getTechnique(0)->getPass(0)->setVertexProgram(
                                                                           baseWhiteNoLighting->getTechnique(1)->getPass(0)->getVertexProgram()->getName());
        baseWhiteNoLighting->getTechnique(0)->getPass(0)->setFragmentProgram(
                                                                             baseWhiteNoLighting->getTechnique(1)->getPass(0)->getFragmentProgram()->getName());
    }
    
    return true;
}

////////////////////////////////////////////////

void OgreApplication::terminateRTShaderSystem()
{
    mShaderGenerator->removeSceneManager(mSceneManager);
    
    // Restore default scheme.
    Ogre::MaterialManager::getSingleton().setActiveScheme(Ogre::MaterialManager::DEFAULT_SCHEME_NAME);
    
    // Unregister the material manager listener.
    if (mMaterialMgrListener != NULL)
    {
        Ogre::MaterialManager::getSingleton().removeListener(mMaterialMgrListener);
        delete mMaterialMgrListener;
        mMaterialMgrListener = NULL;
    }
    
    // Finalize RTShader system.
    if (mShaderGenerator != NULL)
    {
        Ogre::RTShader::ShaderGenerator::finalize();
        mShaderGenerator = NULL;
    }
}

////////////////////////////////////////////////

void OgreApplication::initializeRenderer(void* uiWindow, void* uiView, unsigned int width, unsigned int height)
{
    OgreAssert(!mRoot, "Existing root");
    OgreAssert(!mRenderWindow, "Existing render window");
    OgreAssert(!mSceneManager, "Existing scene manager");
    
    mRoot = new Ogre::Root("", mResourcesRoot + "ogre.cfg");
    m_StaticPluginLoader.load();
    
    Ogre::NameValuePairList params;
    params["colourDepth"] = "32";
    params["contentScaleFactor"] = "2.0";
    params["FSAA"] = "0";
    params["Video Mode"] = Ogre::StringConverter::toString(width) + "x" + Ogre::StringConverter::toString(height);
    params["externalWindowHandle"] = Ogre::StringConverter::toString((unsigned long)uiWindow);
    params["externalViewHandle"] = Ogre::StringConverter::toString((unsigned long)uiView);
    params["externalViewControllerHandle"] = Ogre::StringConverter::toString(NULL);
    params["iosVersion"] = Ogre::StringConverter::toString("7.0");
    
    // Initialize w/o creating a renderwindow.
    mRoot->initialise(false, "");
    
    // Create the window and attach it to the given UI stuffs.
    mRenderWindow = mRoot->createRenderWindow("",100,100,false,&params);
    
    mSceneManager = mRoot->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
}

void OgreApplication::terminateRenderer()
{
    if (mCameraController) delete mCameraController;
    if (mEffectManager)    delete mEffectManager;
    
    mRoot->queueEndRendering();
    
    m_StaticPluginLoader.unload();
    
    delete mRoot;
    mRoot = 0;
    
    mRenderWindow = 0;
    mSceneManager = 0;
}

////////////////////////////////////////////////


ShaderGeneratorTechniqueResolverListener::ShaderGeneratorTechniqueResolverListener(Ogre::RTShader::ShaderGenerator* pShaderGenerator)
{
    mShaderGenerator = pShaderGenerator;
}

////////////////////////////////////////////////

Ogre::Technique* ShaderGeneratorTechniqueResolverListener::handleSchemeNotFound(unsigned short schemeIndex, const Ogre::String& schemeName, Ogre::Material* originalMaterial, unsigned short lodIndex, const Ogre::Renderable* rend)
{
    Ogre::Technique* generatedTech = NULL;
    
    // Case this is the default shader generator scheme.
    if (schemeName == Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME)
    {
        bool techniqueCreated;
        
        // Create shader generated technique for this material.
        techniqueCreated = mShaderGenerator->createShaderBasedTechnique(
                                                                        originalMaterial->getName(),
                                                                        Ogre::MaterialManager::DEFAULT_SCHEME_NAME,
                                                                        schemeName);
        
        // Case technique registration succeeded.
        if (techniqueCreated)
        {
            // Force creating the shaders for the generated technique.
            mShaderGenerator->validateMaterial(schemeName, originalMaterial->getName());
            
            // Grab the generated technique.
            Ogre::Material::TechniqueIterator itTech = originalMaterial->getTechniqueIterator();
            
            while (itTech.hasMoreElements())
            {
                Ogre::Technique* curTech = itTech.getNext();
                
                if (curTech->getSchemeName() == schemeName)
                {
                    generatedTech = curTech;
                    break;
                }
            }				
        }
    }
    
    return generatedTech;
}


////////////////////////////////////////////////
////////////////////////////////////////////////


void OgreApplication::start(void* uiWindow, void* uiView, unsigned int width, unsigned int height, const std::string myResourcesPath, const std::string documentsPath)
{
    initializeRenderer(uiWindow, uiView, width, height);
    createCameraAndViewport();
    
    loadResources(myResourcesPath, documentsPath);
    
    initializeRTShaderSystem();
    
	mRenderWindow->setActive(true);
    
    createScene();
    
    mRoot->getRenderSystem()->_initRenderTargets();
   
    
    // Clear event times
    mRoot->clearEventTimes();
}

////////////////////////////////////////////////

bool OgreApplication::isStarted()
{
    return mRoot != 0;
}

////////////////////////////////////////////////

void OgreApplication::stop()
{
    terminateRTShaderSystem();
    
    terminateRenderer();
}

void OgreApplication::createCameraAndViewport()
{
    OgreAssert(mSceneManager, "NULL scene manager");
    OgreAssert(!mViewport, "Existing viewport");
	
    Ogre::Camera* camera = mSceneManager->createCamera("Camera");
    camera->setNearClipDistance(1);
    
	mViewport = mRenderWindow->addViewport(camera);
	mViewport->setBackgroundColour(Ogre::ColourValue(0.8f, 0.7f, 0.6f, 1.0f));
	mViewport->setCamera(camera);
    
    mCameraController = new CameraController(mSceneManager, camera);
}

////////////////////////////////////////////////

void OgreApplication::loadResources(const std::string myResourcesPath, const std::string documentsPath)
{
    Ogre::String secName, typeName, archName;
	Ogre::ConfigFile cf;
    cf.load(mResourcesRoot + "resources.cfg");
    
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            
            // OS X does not set the working directory relative to the app,
            // In order to make things portable on OS X we need to provide
            // the loading with it's own bundle path location
            if (!Ogre::StringUtil::startsWith(archName, "/", false)) // only adjust relative dirs
                archName = Ogre::String(mResourcesRoot + archName);
            
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
    // App folder is not in default resources, has to be added manually
     Ogre::ResourceGroupManager::getSingleton().addResourceLocation(myResourcesPath,"FileSystem",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    // Documents folder is not in default resources, has to be added manually
    Ogre::ResourceGroupManager::getSingleton().addResourceLocation(documentsPath,"FileSystem",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    
}

////////////////////////////////////////////////

void OgreApplication::createScene()
{
//    cInstance = Ogre::CompositorManager::getSingleton().addCompositor(mViewport,                                                                                                                       "PostProcess/Gamma");
//
//    Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewport, "PostProcess/Gamma", true);
    
//    new EffectManager(mRenderWindow, mSceneManager, mCameraController->_getCamera());
//    mEffectManager = EffectManager::getSingletonPtr();
    
    mSceneManager->setAmbientLight(Ogre::ColourValue(0.1f, 0.1f, 0.1f));
	mSceneManager->setSkyBox(true, "Examples/SpaceSkyBox");
    
    new ModelImporter();
    
    ModelImporter::getSingleton().loadObj("monkey.obj");
    ModelImporter::getSingleton().createMesh("monkeyMesh", "Textured_shaded");
    
    // multiple objects
    const int maxObjCount = 10;
    
    for(int i = 0; i < maxObjCount; i++)
    {
        // objects
        std::string name("Entity");
        
        // convert counter to string
        std::ostringstream ss;
        ss << i;
        std::string iAsString(ss.str());
        name += iAsString;
        
        auto tempNode = ModelImporter::getSingleton().createEntityAndNode(mSceneManager, mSceneManager->getRootSceneNode(), name, "monkeyMesh");
        tempNode->setPosition(Ogre::Vector3(i * 120 - (maxObjCount / 2) * 120,0, -300 ));
        tempNode->setScale(Ogre::Vector3(30,30,30));
        
        
        // red point light
        
        std::string lightName("RedLight");
        lightName += iAsString;
        
        auto redLight = mSceneManager->createLight(lightName);
        redLight->setAttenuation(70, 1.0f, 0.007f, 0.0f);
        
        redLight->setType(Ogre::Light::LT_POINT);
        redLight->setDiffuseColour(Ogre::Real(1), Ogre::Real(0), Ogre::Real(0));
        
        lightName += "node";
        
        auto tmpLightNode = tempNode->createChildSceneNode(lightName);
        lightName += "pivot";
        
        // attach the light to a pivot node
        auto tmpLightPivotNode = tmpLightNode->createChildSceneNode(lightName);
        tmpLightPivotNode->attachObject(redLight);
        tmpLightPivotNode->translate(Ogre::Vector3(0,0,3));
        
        mEntityNodes.push_back(tempNode);
        mLightNodes.push_back(tmpLightNode);
    }
    
//    mEffectManager->addPostProcessEffect(PPE_GrayScale);
}

////////////////////////////////////////////////

void OgreApplication::update(const double timeSinceLastFrame)
{
    mSceneManager->setSkyBoxEnabled(true);
	m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    mCameraController->update(timeSinceLastFrame);
    
    for(auto &l : mLightNodes)
    {
        l->yaw(Ogre::Radian(2 * timeSinceLastFrame) );
    }
}

////////////////////////////////////////////////

void OgreApplication::draw(const double timeSinceLastFrame)
{
    mRoot->renderOneFrame((Ogre::Real)timeSinceLastFrame);
}

////////////////////////////////////////////////

void OgreApplication::selectEntity(const int absScreenX, const int absScreenY )
{
    if(mCameraController->getIsAnimating()) // disable input while animating
        return;
    
    // resetting previous selected entity
    Ogre::Vector3 prevCameraNodePos(Ogre::Vector3::ZERO);
    
    mLastSelectedEntity = mSelectedEntity;
    if (mLastSelectedEntity != NULL)
    {
        //get entity by casting movable
        Ogre::Entity* entity = static_cast<Ogre::Entity*>(mSelectedEntity->getParentSceneNode()->getAttachedObject(0));
        entity->setMaterialName(m_sSelectedEntityPreviousMaterialName);
        mSelectedEntity = NULL;
        m_sSelectedEntityPreviousMaterialName = "";
    }
    
    int width = mViewport->getActualWidth();
    int height = mViewport->getActualHeight();
    
    Ogre::Ray touchRay =  mCameraController->getCamera()->getCameraToViewportRay(absScreenX/float(width),
                                                                                                absScreenY/float(height));
    
    Ogre::RaySceneQuery* rayScnQuery(mSceneManager->createRayQuery(Ogre::Ray()));
    
    rayScnQuery->setRay(touchRay);
    rayScnQuery->setSortByDistance(true);
    
    Ogre::RaySceneQueryResult& result = rayScnQuery->execute();
    Ogre::RaySceneQueryResult::iterator iter = result.begin();
    
    if(iter != result.end() && iter->movable)
    {
        mSelectedEntity = iter->movable;
        
        Ogre::SceneNode* selectedEntityNode = mSelectedEntity->getParentSceneNode();
        
        if(mLastSelectedEntity == NULL || mLastSelectedEntity != mSelectedEntity)
        {
            mCameraController->setCameraMode(CMode_Freefly);
            mCameraController->animateLookatPosition(mSelectedEntity->getWorldBoundingBox().getCenter());
            mCameraController->animateMoveToPosition(selectedEntityNode->getPosition() + Ogre::Vector3(0,0,                                                                                      iter->movable->getWorldBoundingBox().getSize().length() * 2.0f));
        }
        
        // get entity by casting movable
        Ogre::Entity* entity = static_cast<Ogre::Entity*>(selectedEntityNode->getAttachedObject(0));
        
        // saving previous material, if the same entity was not selected already
        m_sSelectedEntityPreviousMaterialName = entity->getSubEntity(0)->getMaterialName();
        
        // apply custom material to selected entity
        entity->setMaterialName("Basic_yellow");
    }
    else
    {
        mCameraController->setCameraMode(CMode_Freefly);
        if (mLastSelectedEntity != NULL)
        {
            mCameraController->animateLookatPosition(mLastSelectedEntity->getWorldBoundingBox().getCenter());
            mCameraController->animateMoveToPosition(Ogre::Vector3::ZERO);
        }
    }
    
    mSceneManager->destroyQuery(rayScnQuery);
}

////////////////////////////////////////////////

void OgreApplication::pinch(const float velocity)
{
    mCameraController->zoom(velocity);
}

void OgreApplication::touchPressed(const float absX, const float absY)
{
    if (mCameraController->isOrbitCameraAutoRotating())
        mCameraController->stopOrbitCameraAutoRotation();
    else
        selectEntity(absX, absY);
    
    return true;
}

void OgreApplication::touchReleased()
{
    mCameraController->touchReleased();
    return true;
}

void OgreApplication::touchMoved(const float relativeX, const float relativeY, const int touchCount)
{
    if (touchCount == 1)
    {
        mCameraController->rotateCamera(relativeX * 2,relativeY * 2,mSelectedEntity);
    }
    else if (touchCount == 2)
    {
        mCameraController->moveCamera(relativeX, relativeY);
    }
    return true;
}

///////////////////////////////////////////

void OgreApplication::setEntitiesTexture(std::string imageName)
{
    std::string path(Ogre::macBundlePath());

    path += "/";
    path += imageName;
    path += ".jpg";
    
    std::cout << "imagePath on loading: " << path << std::endl;
   
    Ogre::TexturePtr texture = Ogre::TextureManager::getSingletonPtr()->load(imageName + ".jpg", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    
    Ogre::Entity* entity = static_cast<Ogre::Entity*>(mEntityNodes[0]->getAttachedObject(0));
    
    Ogre::String materialName = entity->getMesh()->getSubMesh(0)->getMaterialName();
    
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->getByName(materialName);
    if (!material.isNull())
    {
        material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTexture(texture);
        material->load();
        entity->setMaterial(material);
    }
    
}

///////////////////////////////////////////

void OgreApplication::resetCamera()
{
    if (mSelectedEntity != NULL)
    {
        Ogre::Entity* entity = static_cast<Ogre::Entity*>(mSelectedEntity->getParentSceneNode()->getAttachedObject(0));
        
        entity->setMaterialName(m_sSelectedEntityPreviousMaterialName);
    }
    
    mLastSelectedEntity = mSelectedEntity;
    mSelectedEntity = NULL;
    mCameraController->reset();
}

///////////////////////////////////////////

void OgreApplication::addPostProcessEffect(std::string effectname)
{
    if (m_bPostProcess == false)
    {
        Ogre::CompositorManager::getSingleton().addCompositor(mViewport,                                                                                                         "PostProcess/" + effectname);
    
        Ogre::CompositorManager::getSingleton().setCompositorEnabled(mViewport, "PostProcess/" + effectname, true);
        m_bPostProcess = true;
    }
    else
    {
        Ogre::CompositorManager::getSingleton().removeCompositor(mViewport, "PostProcess/" + effectname);
        m_bPostProcess = false;
    }
}

//////////////////////////////////////////