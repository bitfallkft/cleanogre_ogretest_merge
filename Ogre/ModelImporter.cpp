
#include "ModelImporter.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>

#include <OgreMesh.h>
#include <OgreMeshManager.h>

#include <OgreHardwareBufferManager.h>
#include <OgreRenderSystem.h>
#include <OgreRoot.h>
#include <OgreSubMesh.h>
#include <OgreSceneManager.h>
#include <OgreEntity.h>
#include <macUtils.h>

#include <OgreHighLevelGpuProgramManager.h>

#include "OgreHelper.h"

using namespace std;

namespace Ogre
{
    template<> ModelImporter* Ogre::Singleton<ModelImporter>::msSingleton = 0;
};


bool ModelImporter::loadObj(const std::string objName)
{
    bool isVertexNormalsInObj = false;
    int vertexNormalCounter = 0;
    
    bbMinVertices = Ogre::Vector3::ZERO;
    bbMaxVertices = Ogre::Vector3::ZERO;
    
    std::string path(Ogre::macBundlePath());
    path += "/media/models/";
    path += objName;
    std::cout << "Loading .obj: " << path << std::endl;
    
    ifstream modelstream;
    modelstream.clear();
    modelstream.open(path.c_str());
    
    if (modelstream.fail())
    {
        return false;
    }
    
    if (modelstream.is_open())
    {
        string line;
        while ( getline (modelstream,line) )
        {
            istringstream iss(line);
            vector<string> tokens;
            
            // tokenizing on spaces
            copy(istream_iterator<string>(iss),
                 istream_iterator<string>(),
                 back_inserter<vector<string> >(tokens));
            
            if(line.length() > 0)
            {
				if(tokens[0].compare("v") == 0) // vertices
                {
                    float x = atof(tokens[1].c_str());
                    float y = atof(tokens[2].c_str());
                    float z = atof(tokens[3].c_str());
                    Ogre::Vector3 v(x,y,z);
                    Vertex vert(v, Ogre::Vector3::ZERO);
                    
                    vertices.push_back(vert);
                    
                    // BoundingBox min
                    if (x < bbMinVertices.x)
                        bbMinVertices.x = x;
                    
                    if (y < bbMinVertices.y)
                        bbMinVertices.y = y;
                    
                    if (z < bbMinVertices.z)
                        bbMinVertices.z = z;
                    
                    // BoundingBox max
                    if (x > bbMaxVertices.x)
                        bbMaxVertices.x = x;
                    
                    if (y > bbMaxVertices.y)
                        bbMaxVertices.y = y;
                    
                    if (z > bbMaxVertices.z)
                        bbMaxVertices.z = z;
				}
                else if(tokens[0].compare("f") == 0)    // faces
                {
                    // -1 is important, .obj indexes faces from 1
                    indices.push_back( atoi(tokens[1].c_str()) - 1);
                    indices.push_back( atoi(tokens[2].c_str()) - 1);
                    indices.push_back( atoi(tokens[3].c_str()) - 1);
                

				} else if(tokens[0].compare("o") == 0)  // model name
                {
                    string name;
					copy(line.begin() + 2, line.end(),name.begin());
                    
                    meshName = name;
				}
                else if(tokens[0].compare("vn") == 0)   // vertex normals
                {
                    isVertexNormalsInObj = true;
                    
                    float x = atof(tokens[1].c_str());
                    float y = atof(tokens[2].c_str());
                    float z = atof(tokens[3].c_str());
                    
                    Ogre::Vector3 vn(x,y,z);
                    
                    vertices[vertexNormalCounter].Normal = vn;
                    
                    ++vertexNormalCounter;
                }
            }
        }
        modelstream.close();
    }
    else return false;
    
    
    if (!isVertexNormalsInObj)// calculating vertex normals when not provided in obj
    {
        for (int i = 0; i < indices.size() / 3; i++)
        {
            Ogre::Vector3 firstvec = vertices[indices[i*3+1]].Position-vertices[indices[i*3]].Position;
            Ogre::Vector3 secondvec = vertices[indices[i*3]].Position-vertices[indices[i*3+2]].Position;
            Ogre::Vector3 normal = secondvec.crossProduct(firstvec);
            normal.normalise();
            vertices[indices[i * 3]].Normal += normal;
            vertices[indices[i * 3 + 1]].Normal += normal;
            vertices[indices[i * 3 + 2]].Normal += normal;
        }
        
        // normalize vertex normals
        for (int i = 0; i < vertices.size(); i++)
            vertices[i].Normal.normalise();
    }
    return true;
}

////////////////////////////////////////////////////////////

void ModelImporter::createMesh(const std::string meshName, const std::string materialName)
{
    Ogre::MeshPtr ogreMesh;
    ogreMesh = Ogre::MeshManager::getSingleton().createManual(meshName,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    Ogre::AxisAlignedBox vaab;
    
    Ogre::SubMesh* subMesh = ogreMesh->createSubMesh();
    
    // fallback material if the material from material files cannot be loaded
    if (Ogre::MaterialManager::getSingleton().getByName("fallbackMaterial").isNull()) {
        Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("fallbackMaterial", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        mat->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_AMBIENT);
    }
    
    if (materialName.length()>0) {
    if ( Ogre::ResourceGroupManager::getSingleton().resourceExists(Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, "Basic_colored.material")){
            subMesh->setMaterialName(materialName);
        }else{
            subMesh->setMaterialName("fallbackMaterial");
        }
        
    }else{
        subMesh->setMaterialName("fallbackMaterial");
    }
    
    subMesh->useSharedVertices = false;
    
    subMesh->indexData->indexCount = indices.size();
    subMesh->indexData->indexStart = 0;
    subMesh->vertexData = new Ogre::VertexData;
    Ogre::VertexData *vertexData = subMesh->vertexData;
    
    vertexData->vertexCount = vertices.size();
    Ogre::VertexDeclaration* decl = vertexData->vertexDeclaration;
    size_t offset = 0;
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_DIFFUSE);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    
    Ogre::HardwareVertexBufferSharedPtr vertexBuf =
    Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(offset, vertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC, true);
    Ogre::HardwareIndexBufferSharedPtr indexBuf =
    Ogre::HardwareBufferManager::getSingleton().createIndexBuffer(Ogre::HardwareIndexBuffer::IT_16BIT, indices.size(), Ogre::HardwareBuffer::HBU_STATIC, true);
    
    float* vertexBufPtr = static_cast<float*>(vertexBuf->lock(Ogre::HardwareBuffer::HBL_NORMAL));
    Ogre::Vector3 color = Ogre::Vector3(0.5,0.5,0.5);
    for (int j = 0; j < vertices.size(); j++) {
        memcpy(vertexBufPtr, &vertices[j].Position.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
        
        // Normals
        memcpy(vertexBufPtr, &vertices[j].Normal.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
        
        memcpy(vertexBufPtr, &color.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
    }
    
    vaab.setExtents(bbMinVertices,bbMaxVertices);
    
    vertexBuf->unlock();
    Ogre::VertexBufferBinding* bind = subMesh->vertexData->vertexBufferBinding;
    bind->setBinding(0, vertexBuf);
    
    unsigned short * indexBufPtr = static_cast<unsigned short*>(indexBuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));
    
    for (int j = 0; j < indices.size(); j++) {
        *indexBufPtr++ = indices[j];
    }
    
    indexBuf->unlock();
    subMesh->indexData->indexBuffer = indexBuf;
    
    ogreMesh->_setBounds(vaab);
    float distance = 1;
    distance = distance/2.0;
    ogreMesh->_setBoundingSphereRadius(distance);
    ogreMesh->setBackgroundLoaded(false);
    ogreMesh->load();
}

/////////////////////////////////////////////////////

Ogre::SceneNode* ModelImporter::createEntityAndNode(Ogre::SceneManager* sMgr, Ogre::SceneNode* parentNode, std::string entityName, const std::string meshName)
{
    auto mesh = sMgr->createEntity(entityName, meshName);
    entityName += "Node";
    
    auto node = parentNode->createChildSceneNode(entityName);
    node->attachObject(mesh);
    
    return node;
}

//////////////////////////////////////////////////////

Ogre::SceneNode* ModelImporter::createMeshAndNode(Ogre::SceneManager* sMgr, Ogre::SceneNode* parentNode, const Ogre::Vector3 pos, const std::string materialName)
{
    Ogre::MeshPtr ogreMesh;
    Ogre::SceneNode *sceneNode = parentNode->createChildSceneNode("MyNode");
    ogreMesh = Ogre::MeshManager::getSingleton().createManual("MyMesh",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    Ogre::AxisAlignedBox vaab;

    Ogre::SubMesh* subMesh = ogreMesh->createSubMesh();
    if (Ogre::MaterialManager::getSingleton().getByName("testMaterialForObjects").isNull()) {
        Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create("testMaterialForObjects", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        mat->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_AMBIENT);
    }

    Ogre::MaterialPtr testBSMaterial = Ogre::MaterialManager::getSingleton().getByName(materialName);

    if (materialName.length()>0) {
        if (!Ogre::MaterialManager::getSingleton().getByName(materialName).isNull()) {
            subMesh->setMaterialName(materialName);
        }else{
            subMesh->setMaterialName("testMaterialForObjects");
        }
    
    }else{
        subMesh->setMaterialName("testMaterialForObjects");
    }

    subMesh->useSharedVertices = false;

    subMesh->indexData->indexCount = indices.size();
    subMesh->indexData->indexStart = 0;
    subMesh->vertexData = new Ogre::VertexData;
    Ogre::VertexData *vertexData = subMesh->vertexData;

    vertexData->vertexCount = vertices.size();
    Ogre::VertexDeclaration* decl = vertexData->vertexDeclaration;
    size_t offset = 0;
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_DIFFUSE);
    offset+= Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    
    Ogre::HardwareVertexBufferSharedPtr vertexBuf =
    Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(offset, vertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC, true);
    Ogre::HardwareIndexBufferSharedPtr indexBuf =
    Ogre::HardwareBufferManager::getSingleton().createIndexBuffer(Ogre::HardwareIndexBuffer::IT_16BIT, indices.size(), Ogre::HardwareBuffer::HBU_STATIC, true);

    float* vertexBufPtr = static_cast<float*>(vertexBuf->lock(Ogre::HardwareBuffer::HBL_NORMAL));
    Ogre::Vector3 color = Ogre::Vector3(0.5,0.5,0.5);
    for (int j = 0; j < vertices.size(); j++) {
        // position
        memcpy(vertexBufPtr, &vertices[j].Position.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
        
        // normals
        memcpy(vertexBufPtr, &vertices[j].Normal.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
        
        // color
        memcpy(vertexBufPtr, &color.x, sizeof(Ogre::Vector3));
        vertexBufPtr += 3;
    }
    
    vaab.setExtents(bbMinVertices,bbMaxVertices);
    
    vertexBuf->unlock();
    Ogre::VertexBufferBinding* bind = subMesh->vertexData->vertexBufferBinding;
    bind->setBinding(0, vertexBuf);

    unsigned short * indexBufPtr = static_cast<unsigned short*>(indexBuf->lock(Ogre::HardwareBuffer::HBL_DISCARD));

    for (int j = 0; j < indices.size(); j++) {
        *indexBufPtr++ = indices[j];
    }

    indexBuf->unlock();
    subMesh->indexData->indexBuffer = indexBuf;

    ogreMesh->_setBounds(vaab);
    float distance = 1;
    distance = distance/2.0;
    ogreMesh->_setBoundingSphereRadius(distance);
    ogreMesh->setBackgroundLoaded(false);
    ogreMesh->load();

    Ogre::Entity *tempObjectEntity;
    try {
        tempObjectEntity = sMgr->createEntity("MyMesh", ogreMesh);
    }catch (Ogre::Exception &e){
        std::cout<<e.getFullDescription()<<std::endl;
        std::cout<<e.getSource()<<std::endl;
    }
    tempObjectEntity->setVisible(true);
    tempObjectEntity->setCastShadows(false);
    sceneNode->attachObject(tempObjectEntity);
    sceneNode->setScale(Ogre::Vector3(10,10,10));
    sceneNode->showBoundingBox(false);

    return sceneNode;
}