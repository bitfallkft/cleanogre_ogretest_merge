//
//  CameraController.h
//  OgreTest
//
//  Created by Dev on 2/21/14.
//
//

// To make easier extending camera functionality
//
// 2 structures used for different purposes
//
// Orbit camera:
//
//   YawNode        // root
//      |
//   PitchNode
//      |
//   RollNode
//      |
//   CameraNode
//
// Free Camera:
//
//   YawNode        // root
//      |
//   PitchNode
//      |
//   RollNode
//      |
//   CameraNode
//      |
//   TargetNode     // Camera looks at this node

#ifndef OgreTest_CameraHandler_h
#define OgreTest_CameraHandler_h

#include <OgreCamera.h>
#include <OgreSceneManager.h>

enum ECameraMode
{
    CMode_Freefly,
    CMode_Orbit
};

const Ogre::Vector3     DEFAULT_POSITION        = Ogre::Vector3::ZERO;
const Ogre::Vector3     DEFAULT_LOOKATPOSITION  = Ogre::Vector3(0,0,-60);
const float             EPSILON                 = 1e-06;   // values under this considered as zero

////////////////////////////////////////////

class CameraController
{
private:
    Ogre::SceneManager*     mpSceneMgr;
    Ogre::Camera*           mpCamera;
    ECameraMode             mECurrentCameraMode;
    
    Ogre::SceneNode*        mpOrbitYawNode;
    Ogre::SceneNode*        mpOrbitPitchNode;
    Ogre::SceneNode*        mpOrbitRollNode;
    Ogre::SceneNode*        mpOrbitCameraNode;
    
    Ogre::SceneNode*        mpFreeYawNode;
    Ogre::SceneNode*        mpFreePitchNode;
    Ogre::SceneNode*        mpFreeRollNode;
    Ogre::SceneNode*        mpFreeCameraNode;
    Ogre::SceneNode*        mpFreeTargetNode;
    
    bool                    mbIsAnimatingLookat;
    bool                    mbIsAnimatingMovement;
    
    // goal positions for animations, both in WORLD space
    Ogre::Vector3           mvLookatTargetPosition;
    Ogre::Vector3           mvTargetPosition;
    
    float                   mfLookatDamping;
    float                   mfMovementDamping;
    
    float                   mfMovementTime;
    float                   mfMovementRate;
    float                   mfMovementSlice;
    
    bool                    mbIsOrbitRotationTriggered;
    
    bool                    mbIsOrbitAutoRotationTriggered;
    Ogre::Vector2           mvOrbitCameraAutoRotationSpeed;
    
    
    /////////////////////////////////
    
    void                    _init();
    void                    _cleanup();
    void                    _smoothLookatTarget(const double timeSinceLastFrame);
    void                    _animateToTargetPosition(const double timeSinceLastFrame);
    
public:
    CameraController(Ogre::SceneManager* scnMgr, Ogre::Camera* camera);
    virtual ~CameraController();
    
    void setCameraMode(const ECameraMode mode, const Ogre::Vector3 &targetPosition = Ogre::Vector3::ZERO);
    
    // the direction depends on the camera mode
    void rotateYaw(const Ogre::Radian &angle);
    void rotatePitch(const Ogre::Radian &angle);
    
    // orbit camera orbits around the target at this distance
    void moveOrbitCameraZ(const Ogre::Real &value);
    void moveOrbitCamera(const Ogre::Vector3 &value);
    
    // for debugging
    void printOrbitCameraStats(const std::string message = "");
    void printFreeCameraStats(const std::string message = "");
    
    void setLookatTargetPosition(const Ogre::Vector3 &pos);
    void setPosition(const Ogre::Vector3 &pos);
    const Ogre::Vector3& getPosition() const;
 
    bool getIsAnimating() const;
    
    const Ogre::Camera* getCamera() const;
    
    // only if you really need
    Ogre::Camera* _getCamera() const;
    
    void resetOrientation();
    void reset();
    
    void update(const double timeSinceLastFrame);
    
    void rotateCamera(const int relativeX, const int relativeY,
                      const Ogre::MovableObject* selectedEntity);
    void moveCamera(const int relativeX, const int relativeY);
    
    bool getIsOrbitRotationTriggered() const;
    
    void animateMoveToPosition(const Ogre::Vector3 &targetPos);
    void animateLookatPosition(const Ogre::Vector3 &targetPos);
    
    // Sets the mvOrbitCameraAutoRotationSpeed to ZERO
    void stopOrbitCameraAutoRotation();
    
    bool isOrbitCameraAutoRotating() const;
    
    void touchReleased();
    
    void zoom(const float direction);
};


#endif
