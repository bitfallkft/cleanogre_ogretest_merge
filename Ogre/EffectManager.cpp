//
//  EffectManager.cpp
//  OgreTest
//
//  Created by Dev on 2/28/14.
//
//

#include "EffectManager.h"
#include <OgreTextureManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreMaterialManager.h>
#include <OgreTechnique.h>
#include <OgreSceneManager.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreRenderWindow.h>

using namespace Ogre;

namespace Ogre
{
    template<> EffectManager* Ogre::Singleton<EffectManager>::msSingleton = 0;
};



EffectManager::EffectManager(Ogre::RenderWindow* window, Ogre::SceneManager* sceneManager, Ogre::Camera* camera)
: mRenderWindow(window)
, mSceneManager(sceneManager)
, mCamera(camera)
, mFullScreenQuad(0)
, mRTTViewPort(0)
, mTextureRenderTarget(0)
{
    _init();
}

EffectManager::~EffectManager()
{
    if (mFullScreenQuad) delete mFullScreenQuad;
}

//////////////////////////////

void EffectManager::_init()
{
    mEffectArray[PPE_GrayScale] = "BW";
    mEffectArray[PPE_Glass] = "Glass";
    mEffectArray[PPE_Gamma] = "Gamma";
}

////////////////////////////////


void EffectManager::addPostProcessEffect(EPostProcessEffect effect)
{
    std::string effectName = mEffectArray[effect];
    if (mRenderTexturePtr.isNull())
    {
        mRenderTexturePtr = Ogre::TextureManager::getSingleton().createManual("RttTex",
                                                                       Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                                       Ogre::TEX_TYPE_2D, mRenderWindow->getWidth(),
                                                                       mRenderWindow->getHeight(),
                                                                       0,
                                                                       Ogre::PF_R8G8B8,
                                                                       Ogre::TU_RENDERTARGET);
        
        mTextureRenderTarget = mRenderTexturePtr->getBuffer()->getRenderTarget();
        mTextureRenderTarget->setAutoUpdated(true);
        
        mRTTViewPort = mTextureRenderTarget->addViewport(mCamera,1,0,0,1,1);
        mRTTViewPort->setClearEveryFrame(true);
        mRTTViewPort->setBackgroundColour(Ogre::ColourValue::ZERO);
        mRTTViewPort->setVisibilityMask(0x04);
    }
    
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName("PostProcess/"+effectName);
    material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName("RttTex");
    
    if (mFullScreenQuad == NULL)
    {
        mFullScreenQuad = new Ogre::Rectangle2D(true);
        mFullScreenQuad->setCorners(-1.0f, 1.0f, 1.0f, -1.0f);
        mFullScreenQuad->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE, 100000.0f * Ogre::Vector3::UNIT_SCALE));
    }
    
    Ogre::SceneNode* mFullScreenSceneNode = mSceneManager->getRootSceneNode()->createChildSceneNode("FullScreenQuadNode");
    mFullScreenSceneNode->attachObject(mFullScreenQuad);
    
    mFullScreenQuad->setMaterial(material->getName());
    mFullScreenQuad->setVisibilityFlags(0x01);
}

//////////////////////////////
