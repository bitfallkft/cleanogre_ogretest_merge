//
//  CameraHandler.cpp
//  OgreTest
//
//  Created by Dev on 2/21/14.
//
//

#include "CameraController.h"
#include "OgreHelper.h"


CameraController::CameraController(Ogre::SceneManager* scnMgr, Ogre::Camera* camera)
: mpSceneMgr(scnMgr)
, mpCamera(camera)
, mpOrbitRollNode(0)
, mpOrbitPitchNode(0)
, mpOrbitYawNode(0)
, mpOrbitCameraNode(0)
, mpFreeYawNode(0)
, mpFreePitchNode(0)
, mpFreeRollNode(0)
, mpFreeCameraNode(0)
, mpFreeTargetNode(0)
, mbIsAnimatingLookat(false)
, mbIsAnimatingMovement(false)
, mfLookatDamping(8.0f)
, mfMovementDamping(1.0f)
, mbIsOrbitRotationTriggered(false)
, mvOrbitCameraAutoRotationSpeed(Ogre::Vector2::ZERO)
, mfMovementTime(10.0f)
, mfMovementRate(0.0f)
, mfMovementSlice(0.0f)
, mbIsOrbitAutoRotationTriggered(false)
{
    _init();
}

////////////////////////////////////

CameraController::~CameraController()
{
    _cleanup();
}

////////////////////////////////////

void CameraController::_init()
{
    mECurrentCameraMode = CMode_Freefly;
    
    mpOrbitYawNode = mpSceneMgr->getRootSceneNode()->createChildSceneNode("OrbitCameraYawNode");
    mpOrbitPitchNode = mpOrbitYawNode->createChildSceneNode("OrbitCameraPitchNode");
    mpOrbitRollNode = mpOrbitPitchNode->createChildSceneNode("OrbitCameraRollNode");
    mpOrbitCameraNode = mpOrbitRollNode->createChildSceneNode("OrbitCameraNode");

    mpOrbitCameraNode->setPosition(0, 0, 60);
    mpOrbitCameraNode->lookAt(Ogre::Vector3::ZERO, Ogre::Node::TS_LOCAL);
    
    mpFreeYawNode = mpSceneMgr->getRootSceneNode()->createChildSceneNode("FreeCameraYawNode");
    mpFreePitchNode = mpFreeYawNode->createChildSceneNode("FreeCameraPitchNode");
    mpFreeRollNode = mpFreePitchNode->createChildSceneNode("FreeCameraRollNode");
    mpFreeCameraNode = mpFreeRollNode->createChildSceneNode("FreeCameraNode");
    mpFreeTargetNode = mpFreeCameraNode->createChildSceneNode("FreeCameraTargetNode");
    
    mpFreeYawNode->setPosition(DEFAULT_POSITION);
    mpFreeTargetNode->setPosition(DEFAULT_LOOKATPOSITION);
    
    mpFreeCameraNode->attachObject(mpCamera);

    mpFreeCameraNode->lookAt(mpFreeTargetNode->getPosition(), Ogre::Node::TS_LOCAL);
}

////////////////////////////////////

void CameraController::_cleanup()
{
    mpSceneMgr->destroySceneNode(mpOrbitYawNode);
    mpSceneMgr->destroySceneNode(mpOrbitPitchNode);
    mpSceneMgr->destroySceneNode(mpOrbitRollNode);
    mpSceneMgr->destroySceneNode(mpOrbitCameraNode);
    
    mpSceneMgr->destroySceneNode(mpFreeYawNode);
    mpSceneMgr->destroySceneNode(mpFreePitchNode);
    mpSceneMgr->destroySceneNode(mpFreeRollNode);
    mpSceneMgr->destroySceneNode(mpFreeCameraNode);
    mpSceneMgr->destroySceneNode(mpFreeTargetNode);
}

////////////////////////////////////

void CameraController::_smoothLookatTarget(const double timeSlice)
{
    Ogre::Vector3 cameraNodeDerivedPosition = mpFreeCameraNode->_getDerivedPosition();
    Ogre::Vector3 cameraTargetNodeDerivedPosition = mpFreeTargetNode->_getDerivedPosition();
    
    // current lookat direction in World space, on X-Z plane
    Ogre::Vector2 currentDirXZ(cameraTargetNodeDerivedPosition.x - cameraNodeDerivedPosition.x,
                               cameraTargetNodeDerivedPosition.z - cameraNodeDerivedPosition.z);
    // current lookat direction in World space, on Y-Z plane
    Ogre::Vector2 currentDirYZ(cameraTargetNodeDerivedPosition.y - cameraNodeDerivedPosition.y,
                               cameraTargetNodeDerivedPosition.z - cameraNodeDerivedPosition.z);
    
    // desired lookat direction from the camera node to the target position in X-Z plane, in world space
    Ogre::Vector2 desiredDirXZ(mvLookatTargetPosition.x - cameraNodeDerivedPosition.x, mvLookatTargetPosition.z - cameraNodeDerivedPosition.z);
    // desired lookat direction from the camera node to the target position in Y-Z plane, in world space
    Ogre::Vector2 desiredDirYZ(mvLookatTargetPosition.y - cameraNodeDerivedPosition.y, mvLookatTargetPosition.z - cameraNodeDerivedPosition.z);
    
    Ogre::Radian yawAngle = -OgreHelper::angleTo2(desiredDirXZ,currentDirXZ);
    Ogre::Radian pitchAngle = OgreHelper::angleTo2(desiredDirYZ,currentDirYZ);
    
    const double dt = timeSlice * mfLookatDamping;
    
    rotateYaw(yawAngle * dt);
    rotatePitch(pitchAngle * dt);
    
    if ( Ogre::Math::Abs(yawAngle.valueRadians()) < 0.0001f
        && Ogre::Math::Abs(pitchAngle.valueRadians()) < 0.0001f
        && !mbIsAnimatingMovement)   // if we are looking at the right directions, and not moving anywhere, finish it
    {
        rotateYaw(yawAngle);
        rotatePitch(pitchAngle);
        mbIsAnimatingLookat = false;
    }
}

////////////////////////////////////


void CameraController::_animateToTargetPosition(const double timeSinceLastFrame)
{
    if (mfMovementSlice < 1.0)
    {
        mfMovementSlice += timeSinceLastFrame * mfMovementRate * mfMovementDamping;
        setPosition(OgreHelper::lerp(mpFreeYawNode->getPosition(), mvTargetPosition, mfMovementSlice));
    }
    
    if (mvTargetPosition.squaredDistance(mpFreeYawNode->getPosition()) < 0.00001f) // if we are close enough
    {
        mpFreeYawNode->setPosition(mvTargetPosition);   // be exactly at the right spot
        mbIsAnimatingMovement = false;
    }
}

////////////////////////////////////

void CameraController::setCameraMode(const ECameraMode mode, const Ogre::Vector3 &targetPosition)
{
    if (mECurrentCameraMode == mode)
        return;

        mpCamera->detachFromParent();
        
        if (mECurrentCameraMode == CMode_Freefly) // Free -> Orbit transition
        {
            // set position
            mpOrbitYawNode->setPosition(targetPosition);
            mpOrbitPitchNode->setPosition(Ogre::Vector3::ZERO);
            mpOrbitRollNode->setPosition(Ogre::Vector3::ZERO);
            
            
            // set orientation
            mpOrbitYawNode->setOrientation(mpFreeYawNode->getOrientation());
            mpOrbitPitchNode->setOrientation(mpFreePitchNode->getOrientation());
            mpOrbitRollNode->setOrientation(mpFreeRollNode->getOrientation());
            
            moveOrbitCamera(mpFreeYawNode->getPosition() - targetPosition);
            
            mpOrbitCameraNode->attachObject(mpCamera);
        }
        else  // Orbit -> Free transition
        {
            // set orientation
            mpFreeYawNode->setOrientation(mpOrbitYawNode->getOrientation());
            mpFreePitchNode->setOrientation(mpOrbitPitchNode->getOrientation());
            mpFreeRollNode->setOrientation(mpOrbitRollNode->getOrientation());
            mpFreeCameraNode->setOrientation(Ogre::Quaternion::IDENTITY);
            
            // set position
            mpFreeYawNode->setPosition(mpOrbitCameraNode->_getDerivedPosition());
            mpFreePitchNode->setPosition(Ogre::Vector3::ZERO);
            mpFreeRollNode->setPosition(Ogre::Vector3::ZERO);
            mpFreeCameraNode->setPosition(Ogre::Vector3::ZERO);
            mpFreeTargetNode->setPosition(Ogre::Vector3(0,0,-60));
            
            mpFreeCameraNode->attachObject(mpCamera);
            
            mpFreeCameraNode->lookAt(mpFreeTargetNode->getPosition(), Ogre::Node::TS_LOCAL);
        }
        
        mECurrentCameraMode = mode;
}

////////////////////////////////////

void CameraController::rotateYaw(const Ogre::Radian& angle)
{
    
    if(mECurrentCameraMode == CMode_Freefly)
    {
        Ogre::Radian pitch = mpFreePitchNode->getOrientation().getPitch();
        int dirMultiplier = pitch >= (Ogre::Radian)(Ogre::Math::PI/2) || pitch <= -(Ogre::Radian)(Ogre::Math::PI/2)  ? -1 : 1;
        mpFreeYawNode->yaw(-angle * dirMultiplier);
    }
    else
    {
        Ogre::Radian pitch = mpOrbitPitchNode->getOrientation().getPitch();
        int dirMultiplier = pitch >= (Ogre::Radian)(Ogre::Math::PI/2) || pitch <= -(Ogre::Radian)(Ogre::Math::PI/2)  ? -1 : 1;       
        mpOrbitYawNode->yaw(-angle * 3 * dirMultiplier);
    }
}

////////////////////////////////////

void CameraController::rotatePitch(const Ogre::Radian& angle)
{
    if(mECurrentCameraMode == CMode_Freefly)
    {
        mpFreePitchNode->pitch(-angle);
    }
    else
    {
        mpOrbitPitchNode->pitch(-angle * 3);
    }
}

////////////////////////////////////


// Orbit camera orbits around the target from this distance
void CameraController::moveOrbitCameraZ(const Ogre::Real &value)
{
    mpOrbitCameraNode->translate(0,0,value);
}

////////////////////////////////////

void CameraController::moveOrbitCamera(const Ogre::Vector3 &pos)
{
    mpOrbitCameraNode->setPosition(pos);
}

////////////////////////////////////

// debugging
void CameraController::printOrbitCameraStats(const std::string message)
{
    using namespace std;
    cout << "--------------------------" << endl << "Free cam stats" << endl;
    
    if(!message.empty())
        cout << message << endl;
    
    cout << "Position of mpFreeYawNode: " << mpFreeYawNode->_getDerivedPosition() << endl;
    cout << "Position of mpFreePitchNode: " << mpFreePitchNode->_getDerivedPosition() << endl;
    cout << "Position of mpFreeRollNode: " << mpFreeRollNode->_getDerivedPosition() << endl;
    cout << "Position of mpFreeCameraNode: " << mpFreeCameraNode->_getDerivedPosition() << endl;
    cout << "Position of mpCamera: " << mpCamera->getPosition() << endl;
    cout << "Position of mpFreeTargetNode: " << mpFreeTargetNode->_getDerivedPosition() << endl;

    
    cout << "Orientation of mpFreeYawNode: " << mpFreeYawNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpFreePitchNode: " << mpFreePitchNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpFreeRollNode: " << mpFreeRollNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpFreeCameraNode: " << mpFreeCameraNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpCamera: " << mpCamera->getOrientation() << endl;
    cout << "Orientation of mpFreeTargetNode: " << mpFreeTargetNode->getOrientation() << endl;
    cout << "--------------------------" << endl;
}

////////////////////////////////////

void CameraController::printFreeCameraStats(const std::string message)
{
    using namespace std;
    cout << "--------------------------" << endl << "Orbit cam stats" << endl;
    
    if(!message.empty())
        cout << message << endl;
    
 
    cout << "Position of mpOrbitYawNode: " << mpOrbitYawNode->_getDerivedPosition() << endl;
    cout << "Position of mpOrbitPitchNode: " << mpOrbitPitchNode->_getDerivedPosition() << endl;
    cout << "Position of mpOrbitRollNode: " << mpOrbitRollNode->_getDerivedPosition() << endl;
    cout << "Position of mpOrbitCameraNode: " << mpOrbitCameraNode->_getDerivedPosition() << endl;
    cout << "Position of mpCamera: " << mpCamera->getPosition() << endl;

    
    cout << "Orientation of mpOrbitYawNode: " << mpOrbitYawNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpOrbitPitchNode: " << mpOrbitPitchNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpOrbitRollNode: " << mpOrbitRollNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpOrbitCameraNode: " << mpOrbitCameraNode->_getDerivedOrientation() << endl;
    cout << "Orientation of mpCamera: " << mpCamera->getOrientation() << endl;
    cout << "--------------------------" << endl;
}

////////////////////////////////////

void CameraController::setPosition(const Ogre::Vector3 &pos)
{
    if (mECurrentCameraMode == CMode_Freefly)
        mpFreeYawNode->setPosition(pos);
    else
        mpOrbitYawNode->setPosition(pos - mpOrbitCameraNode->getPosition());
}

////////////////////////////////////

const Ogre::Vector3& CameraController::getPosition() const
{
    if (mECurrentCameraMode == CMode_Freefly)
        return mpFreeYawNode->getPosition();
    else
        return mpOrbitCameraNode->_getDerivedPosition();
}

////////////////////////////////////

bool CameraController::getIsAnimating() const
{
    return mbIsAnimatingLookat || mbIsAnimatingMovement;
}

////////////////////////////////////

const Ogre::Camera* CameraController::getCamera() const
{
    return mpCamera;
}

////////////////////////////////////

Ogre::Camera* CameraController::_getCamera() const
{
    return mpCamera;
}

////////////////////////////////////

// we only modify the orientation of these nodes
void CameraController::resetOrientation()
{
    printFreeCameraStats("resetoreantation");
    printOrbitCameraStats("resetorientation");
    
    mpOrbitYawNode->resetOrientation();
    mpOrbitPitchNode->resetOrientation();
    
    mpFreeYawNode->resetOrientation();
    mpFreePitchNode->resetOrientation();
}

////////////////////////////////////

void CameraController::update(const double timeSinceLastFrame)
{
    if(mECurrentCameraMode == CMode_Freefly) // we always animate in freefly mode
    {
        if (mbIsAnimatingMovement)
            _animateToTargetPosition(timeSinceLastFrame);
        
        if (mbIsAnimatingLookat)
            _smoothLookatTarget(timeSinceLastFrame);
    }
    
    if ( mbIsOrbitAutoRotationTriggered)
    {
        rotateYaw(Ogre::Radian(mvOrbitCameraAutoRotationSpeed.x * 0.0001));
        rotatePitch(Ogre::Radian(mvOrbitCameraAutoRotationSpeed.y * 0.0001));
        
        if (mvOrbitCameraAutoRotationSpeed.squaredLength() < 2.0f)
            mbIsOrbitAutoRotationTriggered = false;
    }
        
    // make sure it becomes zero sooner or later
    mvOrbitCameraAutoRotationSpeed *= 0.99f;
}

////////////////////////////////////

void CameraController::reset()
{
    mbIsAnimatingLookat = false;
    mbIsAnimatingMovement = false;
    mvOrbitCameraAutoRotationSpeed = Ogre::Vector2::ZERO;
    mbIsOrbitAutoRotationTriggered = false;
    mbIsOrbitRotationTriggered = false;
    
    resetOrientation();
    
    mpOrbitYawNode->resetToInitialState();
    mpOrbitPitchNode->resetToInitialState();
    
    mpFreeYawNode->resetToInitialState();
    mpFreePitchNode->resetToInitialState();
    
    mvLookatTargetPosition = DEFAULT_LOOKATPOSITION;
}

////////////////////////////////////

void CameraController::rotateCamera(const int relativeX, const int relativeY, const Ogre::MovableObject* selectedEntity)
{
    if (mbIsAnimatingLookat || mbIsAnimatingMovement) // disable input while animating
        return;
    
    bool autoRotating = false;
    if (selectedEntity != NULL) // if entity is selected, rotate the camera around the object
    {
        if (!mbIsOrbitRotationTriggered)
        {
            setCameraMode(CMode_Orbit, selectedEntity->getWorldBoundingBox().getCenter());
            mbIsOrbitRotationTriggered = true;
        }
        
        mvOrbitCameraAutoRotationSpeed = Ogre::Vector2(relativeX,relativeY);
    }
    
    rotateYaw(Ogre::Radian(relativeX * 0.001));
    rotatePitch(Ogre::Radian(relativeY * 0.001));
}

////////////////////////////////////

void CameraController::moveCamera(const int x, const int y)
{
    if (mbIsAnimatingLookat || mbIsAnimatingMovement) // disable input while animating
        return;
    
    if (mECurrentCameraMode == CMode_Freefly)
    {
        Ogre::Vector3 vec(x,-y,0);

        // Transform the axes of the relative vector by camera's local axes
        Ogre::Vector3 trans = mpFreeTargetNode->_getDerivedOrientation() * (vec);
        mpFreeYawNode->translate(trans);
    }
}

////////////////////////////////////

bool CameraController::getIsOrbitRotationTriggered() const
{
    return mbIsOrbitRotationTriggered;
}

////////////////////////////////////

void CameraController::animateMoveToPosition(const Ogre::Vector3 &targetPos)
{
    mfMovementSlice = 0.0f;
    mfMovementRate = 1.0/mfMovementTime;
    mbIsAnimatingMovement = true;
    mvTargetPosition = targetPos;
}

////////////////////////////////////

void CameraController::animateLookatPosition(const Ogre::Vector3 &targetPos)
{
    mbIsAnimatingLookat = true;
    mvLookatTargetPosition = targetPos;
}


////////////////////////////////////

void CameraController::stopOrbitCameraAutoRotation()
{
    mvOrbitCameraAutoRotationSpeed = Ogre::Vector2::ZERO;
}

////////////////////////////////////

bool CameraController::isOrbitCameraAutoRotating() const
{
    return mbIsOrbitAutoRotationTriggered;
}

////////////////////////////////////

void CameraController::touchReleased()
{
    mbIsOrbitRotationTriggered = false;
    if (mvOrbitCameraAutoRotationSpeed.length() > EPSILON)
    {
        mbIsOrbitAutoRotationTriggered = true;
    }
}

////////////////////////////////////

void CameraController::zoom(const float direction)
{
    if (mbIsAnimatingLookat || mbIsAnimatingMovement) // disable input while animating
       return;
    
    if (mECurrentCameraMode == CMode_Freefly && Ogre::Math::Abs(direction) > EPSILON)
    {
        mpFreeYawNode->translate((mpFreeTargetNode->_getDerivedPosition() - mpFreeYawNode->getPosition()).normalisedCopy() * direction * 6);
    }
}

////////////////////////////////////