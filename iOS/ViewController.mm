//
//         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                     Version 2, December 2004
//
//  Copyright (C) 2013 Clodéric Mars <cloderic.mars@gmail.com>
//
//  Everyone is permitted to copy and distribute verbatim or modified
//  copies of this license document, and changing it is allowed as long
//  as the name is changed.
//
//             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//   0. You just DO WHAT THE FUCK YOU WANT TO.

#import "ViewController.h"

#import "OgreView.h"
#import "OgreApplication.h"

#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>

// private category
@interface ViewController ()
{
    OgreApplication mApplication;
    // Use of the CADisplayLink class is the preferred method for controlling your animation timing.
    // CADisplayLink will link to the main display and fire every vsync when added to a given run-loop.
    // The NSTimer class is used only as fallback when running on a pre 3.1 device where CADisplayLink
    // isn't available.
    CADisplayLink* mDisplayLink;
    NSDate *mDate;
    double mLastFrameTime;
    // CoreMotion manager (for acccelerometer/compass/gyroscope informations)
    CMMotionManager* mMotionManager;
    
    CGPoint mLastTouchPosition;
}



@property (retain) CMAttitude* mReferenceAttitude;
@property (retain) OgreView* mOgreView;


////////// Texture picker
@property (retain, nonatomic) UIPopoverController *popoverControllerTest;
@property (retain, nonatomic) IBOutlet UIImageView *pickedImage;

@property (strong) UIImagePickerController *imagePickerController;
@property (strong) UIPopoverController *photoPickerPop;


@property (nonatomic, retain) NSTimer *cameraTimer;
@property (nonatomic) NSMutableArray *capturedImages;

//////// ~Texture picker

@end

@implementation ViewController

@synthesize mOgreView;
@synthesize mReferenceAttitude;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        mReferenceAttitude = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imagePickerController = [[[UIImagePickerController alloc] init] autorelease];
    self.imagePickerController.delegate = self;
    self.photoPickerPop = [[[UIPopoverController alloc] initWithContentViewController:self.imagePickerController] autorelease];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startWithWindow:(UIWindow*) window
{
    unsigned int width  = self.view.frame.size.width;
    unsigned int height = self.view.frame.size.height;
    
    mOgreView = [[OgreView alloc] initWithFrame:CGRectMake(0,0,width,height)];
    [self.view addSubview:mOgreView];
    [mOgreView setFrame:CGRectMake(0, 0, 1024, 768)];
    mOgreView.contentScaleFactor = 2.0;
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    mLastFrameTime = 1;
    
    NSString* shaderPath = [[NSBundle mainBundle] pathForResource:@"TestShaderMaterial" ofType:@"material"];
    try
    {
        mApplication.start(window, mOgreView, width, height,
                           [[shaderPath stringByDeletingLastPathComponent] UTF8String],
                           [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path] UTF8String]);
    }
    catch( Ogre::Exception& e )
    {
        std::cerr << "An exception has occurred: " <<
        e.getFullDescription().c_str() << std::endl;
    }
    
    // Linking the ogre view to the render window
    mOgreView.mRenderWindow = mApplication.mRenderWindow;
    
    // Ogre has created an EAGL2ViewController for the provided mOgreView
    // and assigned it as the root view controller of the window
    //
    // Let's first retrieve it
    UIViewController* ogreViewController = window.rootViewController;
    // I want to be the actual root view controller
    window.rootViewController = self;
    
    // add the ogre view as a sub view and add constraints for them to match
    [self.view addSubview:mOgreView];
    [self.view sendSubviewToBack:mOgreView];
    mOgreView.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *views = NSDictionaryOfVariableBindings(mOgreView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mOgreView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mOgreView]|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    [self.view layoutIfNeeded];
    
    // Create a CMMotionManager
    mMotionManager = [[CMMotionManager alloc] init];
    
    [self startMotionListener];
    
    // CADisplayLink is API new to iPhone SDK 3.1. Compiling against earlier versions will result in a warning, but can be dismissed
    // if the system version runtime check for CADisplayLink exists in -initWithCoder:. The runtime check ensures this code will
    // not be called in system versions earlier than 3.1.
    mDate = [[NSDate alloc] init];
    mLastFrameTime = -[mDate timeIntervalSinceNow];
    
    mDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update:)];
    [mDisplayLink setFrameInterval:mLastFrameTime];
    [mDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    mLastTouchPosition.x = 0.0f;
    mLastTouchPosition.y = 0.0f;
    
    [pool release];
}

- (void)stop
{
    [self stopMotionListener];
    
    try
    {
        mApplication.stop();
    }
    catch( Ogre::Exception& e )
    {
        std::cerr << "An exception has occurred: " <<
        e.getFullDescription().c_str() << std::endl;
    }
    
    [mDate release];
    mDate = nil;
    
    [mDisplayLink invalidate];
    mDisplayLink = nil;
    
    
    [_photoPickerPop release];
    [mOgreView release];
}

- (void)update:(id)sender
{
    if(mApplication.isStarted())
    {
		if(mApplication.mRenderWindow->isActive())
		{
            // using ios timer instead of ogre timer
            double currentFrameTime = -[mDate timeIntervalSinceNow];
            double differenceInSeconds = currentFrameTime - mLastFrameTime;
            mLastFrameTime = currentFrameTime;
            
			mApplication.update(differenceInSeconds);
			mApplication.draw(differenceInSeconds);
            
		}
    }
}


// Input

- (IBAction)pinch:(UIPinchGestureRecognizer *)sender
{
    mApplication.pinch(sender.velocity);
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender
{
    CGPoint touchPos = [sender locationInView:self.view];
    
    CGPoint transl = [self vectorFromPoint:mLastTouchPosition toPoint:touchPos];
    
    NSUInteger numberOfTouches = [sender numberOfTouches];
    
    CGFloat squaredTranslX = transl.x*transl.x;
    CGFloat squaredTranslY = transl.y*transl.y;
    
    if (( squaredTranslX > 0.00001 || squaredTranslY > 0.00001 )
        && (mLastTouchPosition.x * mLastTouchPosition.x > 0.00001
            || mLastTouchPosition.y * mLastTouchPosition.y > 0.00001)
        && numberOfTouches == 2)
    {
        mApplication.touchMoved(transl.x, transl.y, 2);
    }
    
    mLastTouchPosition = touchPos;
}

- (IBAction)btnTexturePicker:(UIButton *)sender
{
    [self imageFromDevice:self];
}

- (IBAction)btnTexturePhoto:(UIButton *)sender
{
    [self imageFromCamera:self];
}

- (IBAction)btnResetCamera:(UIButton *)sender
{
    mApplication.resetCamera();
}

- (IBAction)btnPostProcess:(UIButton *)sender
{
    mApplication.addPostProcessEffect();
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPos = [touch locationInView:self.view];
    
    mLastTouchPosition.x = 0;
    mLastTouchPosition.y = 0;
    
    mApplication.touchPressed(touchPos.x*2, touchPos.y*2);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    mApplication.touchReleased();
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if ( [[event allTouches] count] < 2)
    {
        CGPoint touchLocationpoint = [touch locationInView:[self view]];
        CGPoint PrevioustouchLocationpoint = [touch previousLocationInView:[self view]];
        
        CGPoint transl = [self vectorFromPoint:PrevioustouchLocationpoint toPoint:touchLocationpoint];
        
        mApplication.touchMoved(transl.x, transl.y, [[event allTouches] count]);
    }
}

////////////////////

- (void)startMotionListener
{
    [mMotionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryCorrectedZVertical];
    self.mReferenceAttitude = nil;
}

- (void)stopMotionListener {
    self.mReferenceAttitude = nil;
    [mMotionManager stopDeviceMotionUpdates];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self stopMotionListener];
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self startMotionListener];
}


- (void)dealloc
{
    [self stop];
    [super dealloc];
}


-(CGPoint)vectorFromPoint:(CGPoint)firstPoint toPoint:(CGPoint)secondPoint
{
    CGFloat x = secondPoint.x - firstPoint.x;
    CGFloat y = secondPoint.y - firstPoint.y;
    CGPoint result = CGPointMake(x, y);
    return result;
}



//////////////// Texture picker

- (IBAction)imageFromCamera:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] )
    {
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    CGRect frame = CGRectMake((800 - 300 )/2.f - 110.0, 345, 300, 40);
    [self.photoPickerPop presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp  animated:true];
}

- (IBAction)imageFromDevice:(id)sender
{
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    CGRect frame = CGRectMake((800 - 300 )/2.f, 330, 300, 40);
    [self.photoPickerPop presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:true];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    [self.photoPickerPop dismissPopoverAnimated:YES];
    
    self.pickedImage.image = image;
    
    
    //save image:
    NSLocale* currentLocale = [NSLocale currentLocale];
    
    NSString* imageName = [NSString stringWithFormat:@"%@_image",[[NSDate date] descriptionWithLocale:currentLocale]];
    imageName = [imageName stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([self saveImage:image withFileName: imageName ofType:@"jpg" inDirectory:[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path]])
    {
        DLog(@"Successfully saved file: %@", imageName);
    }
    else
    {
        DLog(@"Failed saved file: %@", imageName);
    }
    
    mApplication.setEntitiesTexture([imageName UTF8String]);
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.photoPickerPop dismissPopoverAnimated:YES];
}

// FileManagement

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}

-(BOOL) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:directoryPath])
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:nil])
        {
            DLog(@"could not save the image");
            return false;
        }
    }
    
    if ([[extension lowercaseString] isEqualToString:@"png"])
    {
        NSString *str = [NSString stringWithFormat:@"%@.%@", imageName, @"png"];
        NSString *path = [directoryPath stringByAppendingPathComponent:str];
        
        DLog(@"file path for image: %@",path);
        
        if ([UIImagePNGRepresentation(image) writeToFile:path atomically:YES])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"])
    {
        NSString *str = [NSString stringWithFormat:@"%@.%@", imageName, @"jpg"];
        NSString *path = [directoryPath stringByAppendingPathComponent:str];
        
        DLog(@"file path for image: %@",path);

        if ([UIImageJPEGRepresentation(image, 1.0) writeToFile:path options:NSAtomicWrite error:nil])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        DLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG/png/jpg) ",
              extension);
        return false;
    }
}

- (UIImage*)getImageForName:(NSString*)fileName inDirectory:(NSString*)directoryPath withExtension:(NSString*)extension
{
    NSString *path = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",fileName,extension]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        return NULL;
    }
    NSData *imageData = [NSData dataWithContentsOfFile:path];
    UIImage *image = [UIImage imageWithData:imageData];
    return image;
}

@end
