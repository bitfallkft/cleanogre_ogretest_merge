//
//         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//                     Version 2, December 2004
//
//  Copyright (C) 2013 Clodéric Mars <cloderic.mars@gmail.com>
//
//  Everyone is permitted to copy and distribute verbatim or modified
//  copies of this license document, and changing it is allowed as long
//  as the name is changed.
//
//             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
//    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
//
//   0. You just DO WHAT THE FUCK YOU WANT TO.

#import <UIKit/UIKit.h>

@class OgreView;

@interface ViewController : UIViewController <UINavigationControllerDelegate,
                                            UIImagePickerControllerDelegate>

- (void)startWithWindow:(UIWindow*) window;
- (void)update:(id)sender;
- (void)stop;

// input
- (IBAction)pinch:(UIPinchGestureRecognizer *)sender;
- (IBAction)pan:(UIPanGestureRecognizer *)sender;
- (IBAction)btnTexturePicker:(UIButton *)sender;
- (IBAction)btnTexturePhoto:(UIButton *)sender;
- (IBAction)btnResetCamera:(UIButton *)sender;
- (IBAction)btnPostProcess:(UIButton *)sender;



- (CGPoint)vectorFromPoint:(CGPoint)firstPoint toPoint:(CGPoint)secondPoint;

// Texture picker
- (IBAction)imageFromCamera:(id)sender;
- (IBAction)imageFromDevice:(id)sender;

// FileManagement
-(UIImage *) getImageFromURL:(NSString *)fileURL;
-(BOOL) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath ;
- (UIImage*)getImageForName:(NSString*)fileName inDirectory:(NSString*)directoryPath withExtension:(NSString*)extension;

@end
